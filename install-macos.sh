#!/usr/bin/env bash
brew install zsh
chsh -s /usr/local/bin/zsh
sh -c "$(curl -fsSL https://git.io/get-zi)" --
wget https://gitlab.com/chevdor/dotfiles/-/raw/master/.zshrc -O $HOME/.zshrc
wget https://gitlab.com/chevdor/dotfiles/-/raw/master/.zsh_sccache -O $HOME/.zsh_sccache
touch $HOME/.zsh_secrets
