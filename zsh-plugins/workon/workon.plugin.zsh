# fw/workon
export FW_CONFIG_DIR=$HOME/system/.fw
if [[ -x "$(command -v fw)" ]]; then
  if [[ -x "$(command -v fzf)" ]]; then
    eval $(fw print-zsh-setup -f 2>/dev/null);
  else
    eval $(fw print-zsh-setup 2>/dev/null);
  fi;
fi;

alias wo='workon'
alias rwo='reworkon'
alias nwo='nworkon'
alias fwconfig='code ~/.fw/'
