export RUSTC_VERSION=nightly-2021-03-15;
export PACKAGE=polkadot-runtime;

mkdir -p /tmp/cargo /tmp/out
alias srtool='docker run --name srtool --rm -it -e PACKAGE=$PACKAGE -v /tmp/out:/out -v $PWD:/build -v /tmp/cargo:/cargo-home chevdor/srtool:$RUSTC_VERSION'

function srtl() { docker run --rm -it -e PACKAGE=$1 -v $PWD:/build -v /tmp/cargo:/cargo-home chevdor/srtool:$RUSTC_VERSION $2; }
