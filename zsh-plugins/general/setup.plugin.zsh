whence -p fzf || brew install fzf
whence -p gh || cargo install --locked --git https://github.com/chevdor/gh --tag v0.2.0
whence -p srtool || cargo install --locked --git https://github.com/chevdor/srtool-cli
whence -p tera || cargo install --locked --git https://github.com/chevdor/tera-cli
whence -p just || cargo install just
whence -p eza || cargo install eza
whence -p fw || cargo install fw
whence -p toml || cargo install --git https://github.com/chevdor/toml-cli
whence -p sshq || cargo install --git https://github.com/chevdor/sshq
