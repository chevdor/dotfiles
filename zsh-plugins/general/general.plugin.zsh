#alias ll='ls -AlG'
alias ll='eza -abghHliS --git'
alias du='du -d 1 -h -c'
alias ipfs='ipfs --api /ip4/127.0.0.1/tcp/5001'
alias zshconfig="joe ~/.zshrc; . ~/.zshrc"
alias edit='open -a TextEdit'
alias scan='clamscan --bell -r -i'
alias t=talosctl

# allow comments on the command line
setopt interactivecomments

alias edit='open -a TextEdit'

# Don't save in the history the command prefixed with a space
setopt HIST_IGNORE_SPACE

# alias j='just'
function j() {
    SEARCH=${*};

    if [ $# -gt 0 ]; then
        hit=$(just --dump | sed -nr 's/^([0-9a-zA-Z_\-]+):.*/\1/p' | sort -r | tr ' ' '\n' | fzf -i -1 --query "${SEARCH}")
    else
        hit=$(just --dump | sed -nr 's/^([0-9a-zA-Z_\-]+):.*/\1/p' | sort -r | tr ' ' '\n' | fzf -i -1 )
    fi

    if [ "$hit" ]; then
        just "$hit"
    fi
}
