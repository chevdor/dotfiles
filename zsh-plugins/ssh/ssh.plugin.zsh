h=()
if [[ -r ~/.ssh/config ]]; then
  h=($h ${${${(@M)${(f)"$(cat ~/.ssh/config)"}:#Host *}#Host }:#*[*?]*})
fi
if [[ -r ~/.ssh/known_hosts ]]; then
  h=($h ${${${(f)"$(cat ~/.ssh/known_hosts{,2} || true)"}%%\ *}%%,*}) 2>/dev/null
fi
if [[ $#h -gt 0 ]]; then
  zstyle ':completion:*:ssh:*' hosts $h
  zstyle ':completion:*:slogin:*' hosts $h
fi

function co() {
    SEARCH=${@:-''};
     if [ $SEARCH ]; then
        hit=$(sshq list | fzf -i -1 --query "$SEARCH" --preview 'sshq search {}')
    else
        hit=$(sshq list | fzf -i -1 --preview 'sshq search {}')
    fi
    echo "Connecting to $hit... you may need to insert your Yubikey..."
    ssh $hit
}
