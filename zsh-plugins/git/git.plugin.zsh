# Some aliases for git
alias g='git'
alias gca='git commit -a'
alias gpum='git pull upstream master'
alias ga='git add . && git status'

# A convenient to create new PR branches
pr() {
    # tmsp in the form YYMMDD
    tmsp=$(date '+%y%m%d')

    git checkout -b wk-$tmsp-$1
    git branch | grep wk | sort
}

function gsw() {
    SEARCH=${@:-''};
    if [ $SEARCH ]; then
        git branch --format "%(refname:short)" | sort -r | fzf -i -1 --preview 'git-show-branch {}' --query "$SEARCH" | xargs git switch
    else
        git branch --format "%(refname:short)" | sort -r | fzf -i -1 --preview 'git-show-branch {}' | xargs git switch
    fi
}

alias gco=gsw
alias grv='git remote -v'
alias gch="git for-each-ref --sort=committerdate refs/heads/ --format='%(HEAD) %(color:yellow)%(refname:short)%(color:reset) - %(color:red)%(objectname:short)%(color:reset) - %(contents:subject) - %(authorname) (%(color:green)%(committerdate:relative)%(color:reset))'"
