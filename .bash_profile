. /usr/local/etc/profile.d/z.sh

alias ll='ls -alG'
alias tree='tree -C -A'
export HOMEBREW_EDITOR=subl
export PATH="/usr/local/sbin:$PATH"
export EDITOR=subl

if which rbenv > /dev/null; then eval "$(rbenv init -)"; fi
export GOPATH=/Users/will/gowork

# see https://github.com/jimeh/git-aware-prompt
export GITAWAREPROMPT=~/.bash/git-aware-prompt
source "${GITAWAREPROMPT}/main.sh"
export PS1="\u@\h \W \[$txtcyn\]\$git_branch\[$txtred\]\$git_dirty\[$txtrst\]\$ "

# gitlab from http://narkoz.github.io/gitlab/configuration
export GITLAB_API_ENDPOINT=https://gitlab.com/api/v3