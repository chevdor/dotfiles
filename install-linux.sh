#!/usr/bin/env bash
apt install zsh
chsh -s /usr/local/bin/zsh
wget https://gitlab.com/chevdor/dotfiles/-/raw/master/.zshrc -O $HOME/.zshrc

