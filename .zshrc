# Uncomment the next line and the call to zprof at the end of the file to enable profiling
# zmodload zsh/zprof

# If you come from bash you might have to change your $PATH.
# export PATH=$HOME/bin:/usr/local/bin:$PATH

# You may need to manually set your language environment
export LANG=en_US.UTF-8

export VISUAL=$(which code)
# Preferred editor for local and remote sessions
if [[ -n $SSH_CONNECTION ]]; then
  export EDITOR='joe'
else
  export EDITOR="$VISUAL"
fi

# Compilation flags
# export ARCHFLAGS="-arch x86_64"

export PATH=$HOME/bin:/usr/local/share/npm/bin:/usr/local/bin:/usr/local/opt/gnu-sed/libexec/gnubin:/usr/bin:/bin:/usr/sbin:/sbin:/opt/X11/bin:/usr/local/MacGPG2/bin:/bin
export PATH="${PATH}:${HOME}/.cargo/bin"
export PATH="/usr/local/opt/llvm/bin:$PATH"
export PATH="$HOME/.yarn/bin:$HOME/.config/yarn/global/node_modules/.bin:$PATH"

# export LDFLAGS="-L/usr/local/opt/llvm/lib"
export LDFLAGS="-L/usr/local/opt/openssl@3/lib"
export LLVM_CONFIG_PATH=/usr/local/opt/llvm/bin
# export CPPFLAGS="-I/usr/local/opt/llvm/include"
export CPPFLAGS="-I/usr/local/opt/openssl@3/include"
export LC_ALL=en_US.UTF-8
export LANG=en_US.UTF-8
export OPENSSL_INCLUDE_DIR=$(brew --prefix openssl)/include
export OPENSSL_LIB_DIR=$(brew --prefix openssl)/lib
export OPENSSL_DIR=$(brew --prefix openssl)/include/openssl
export HOMEBREW_NO_AUTO_UPDATE=1
export HOMEBREW_EDITOR=subl
export SSH_AUTH_SOCK=~/.gnupg/S.gpg-agent.ssh		# to use ssh with yubikey


eval $(ssh-agent) > /dev/null

zi_home="${HOME}/.zi"
source "${zi_home}/bin/zi.zsh"
autoload -Uz _zi
(( ${+_comps} )) && _comps[zi]=_zi

# load the agnoster theme
zi light agnoster/agnoster-zsh-theme
setopt prompt_subst

# Two regular plugins loaded without investigating.
zi light zdharma/fast-syntax-highlighting
zi light agkozak/zsh-z
zi light "supercrabtree/k"
zi light b4b4r07/enhancd
zi load zdharma/history-search-multi-word
zi ice blockf
zi light zsh-users/zsh-completions
zi light zsh-users/zsh-autosuggestions

zi snippet OMZ::plugins/dotenv/dotenv.plugin.zsh

autoload -Uz compinit && compinit

autoload -U +X bashcompinit && bashcompinit
complete -o nospace -C /usr/local/bin/mc mc

# load my own snippets
MY_REPO="https://gitlab.com/chevdor/dotfiles/-/raw/master/zsh-plugins"
for plugin (ansible brew docker general git k8s python rust ssh workon yarn); {
  SNIPPET="$MY_REPO/$plugin/$plugin.plugin.zsh"
  zinit snippet $SNIPPET
}

export NVM_DIR="$([ -z "${XDG_CONFIG_HOME-}" ] && printf %s "${HOME}/.nvm" || printf %s "${XDG_CONFIG_HOME}/nvm")"
[ -s "$NVM_DIR/nvm.sh" ] && \. "$NVM_DIR/nvm.sh" --no-use # This loads nvm

# This is assuming you mapped cargo with Users/will/.cargo in /etc/synthetic.conf and rebooted

export RUBY_CONFIGURE_OPTS="--with-openssl-dir=$(brew --prefix openssl@1.1)"
# export PATH="$HOME/.rbenv/bin:$PATH"
eval "$(rbenv init - zsh)"
export PATH="$PATH:/usr/local/lib/ruby/gems/3.0.0/bin"
export PATH="$PATH:/usr/local/lib/ruby/gems/2.7.0/bin"
export PATH="$PATH:/usr/local/sbin"
export PATH="$PATH:/usr/local/opt/ruby/bin"
export LDFLAGS="-L/usr/local/opt/ruby/lib"
export CPPFLAGS="-I/usr/local/opt/ruby/include"

# GPG related section (gpg, pinentry-mac)
export LDFLAGS="-L/usr/local/opt/libffi/lib"
export CPPFLAGS="-I/usr/local/opt/libffi/include"
# export PKG_CONFIG_PATH="/usr/local/opt/libffi/lib/pkgconfig"
export PKG_CONFIG_PATH="/usr/local/opt/openssl@3/lib/pkgconfig"
export PATH="/usr/local/opt/m4/bin:$PATH"
export GUILE_LOAD_PATH="/usr/local/share/guile/site/3.0"
export GUILE_LOAD_COMPILED_PATH="/usr/local/lib/guile/3.0/site-ccache"
export GUILE_SYSTEM_EXTENSIONS_PATH="/usr/local/lib/guile/3.0/extensions"
export GUILE_TLS_CERTIFICATE_DIRECTORY=/usr/local/etc/gnutls/

export SSH_AUTH_SOCK=$(gpgconf --list-dirs agent-ssh-socket)
gpgconf --launch gpg-agent
export GPG_TTY=$(tty)

export PATH="$HOME/.poetry/bin:$PATH"
export PATH="$HOME/scripts/:$PATH"
export APPSUPP="$HOME/Library/Application Support"

# >>> conda initialize >>>
# !! Contents within this block are managed by 'conda init' !!
__conda_setup="$('/usr/local/Caskroom/miniconda/base/bin/conda' 'shell.zsh' 'hook' 2> /dev/null)"
if [ $? -eq 0 ]; then
    eval "$__conda_setup"
else
    if [ -f "/usr/local/Caskroom/miniconda/base/etc/profile.d/conda.sh" ]; then
        . "/usr/local/Caskroom/miniconda/base/etc/profile.d/conda.sh"
    else
        export PATH="/usr/local/Caskroom/miniconda/base/bin:$PATH"
    fi
fi
unset __conda_setup
# <<< conda initialize <<<

export XML_CATALOG_FILES=/usr/local/etc/xml/catalog

# alias gsw='git branch | fzf -i -1 | xargs git switch'
# A function to show details about a branch
#function git_show_branch() {
#    MERGE_BASE_MASTER=$(git merge-base $1 master)
#    echo "Merge-base master: $MERGE_BASE_MASTER"
#    git log -n1 --pretty=medium $MERGE_BASE_MASTER
#    git log -n1 --pretty=medium $1
#}

[ -f ~/.fzf.zsh ] && source ~/.fzf.zsh

# The next line updates PATH for the Google Cloud SDK.
if [ -f '/Users/will/google-cloud-sdk/path.zsh.inc' ]; then . '/Users/will/google-cloud-sdk/path.zsh.inc'; fi

# The next line enables shell command completion for gcloud.
if [ -f '/Users/will/google-cloud-sdk/completion.zsh.inc' ]; then . '/Users/will/google-cloud-sdk/completion.zsh.inc'; fi

# Those are for gtkwave :///
PATH="/Users/will/perl5/bin${PATH:+:${PATH}}"; export PATH;
PERL5LIB="/Users/will/perl5/lib/perl5${PERL5LIB:+:${PERL5LIB}}"; export PERL5LIB;
PERL_LOCAL_LIB_ROOT="/Users/will/perl5${PERL_LOCAL_LIB_ROOT:+:${PERL_LOCAL_LIB_ROOT}}"; export PERL_LOCAL_LIB_ROOT;
PERL_MB_OPT="--install_base \"/Users/will/perl5\""; export PERL_MB_OPT;
PERL_MM_OPT="INSTALL_BASE=/Users/will/perl5"; export PERL_MM_OPT;

# For gsutil to work properly
export CLOUDSDK_PYTHON=python3
export PATH=$PATH:~/go/bin

for f (.zsh_secrets .zsh_sccache); {
  source $HOME/$f
}

# overwritte the default that does not use system
alias fwconfig='code ~/system/.fw/'
export PATH="/usr/local/opt/openssl@3/bin:$PATH"

# use GNU grep and other GNU tools first
export PATH="/usr/local/opt/grep/libexec/gnubin:$PATH"

alias xa='xargs -I{}'
alias gbd='git branch | fzf -m | xargs -I{} git branch -d {}'
alias gbD='git branch | fzf -m | xargs -I{} git branch -D {}'
alias rn='relnot new'
alias drun='docker run --rm -it'
alias rl=ruled-labels

export PATH="/usr/local/opt/libpq/bin:$PATH"
export REPO_SUBSTRATE=/projects/substrate
export REPO_POLKADOT=/projects/polkadot
export REPO_CUMULUS=/projects/cumulus
export PATH=$PATH:/projects/release-engineering/scripts
export PATH="/usr/local/opt/openssl@3/bin:$PATH"
